#!/bin/sh
#
# Install the ssheet_driver, swrap_driver, and associated symlinks.
#

PYTHON_INTERPRETOR="/usr/bin/env python"
if [ $? -ne 0 ]; then
    echo "ERROR:  No python interpreter found."
    exit 1
fi
if [ "$(id -u)" -eq 0 ]; then
    INSTALL_DIR="/usr/local/bin"
else
    INSTALL_DIR="${HOME}/bin"
fi

while [ $# -gt 0 ]; do
    case "$1" in

        -h|--help)
            cat <<EOT
usage:

    $0 {options}

  options:

    -h/--help                     display this information
    -i/--installdir <path>        install commands to this directory (default
                                  is /usr/local/bin for root, ~/bin for other
                                  users)
    -p/--python <cmd>             Python interpretor to use in the scripts
                                  (defaults to "/usr/bin/env python")

EOT
            exit 0
            ;;
        -i|--installdir)
            shift
            if [ $# -eq 0 ]; then
                echo "ERROR:  No path provided with -i/--installdir"
                exit 1
            fi
            if [ ! -d "$1" ]; then
                echo "ERROR:  Directory does not exist: $1"
                exit 1
            fi
            INSTALL_DIR="$1"
            ;;
        -p|--python)
            shift
            if [ $# -eq 0 ]; then
                echo "ERROR:  No argument provided with -p/--python"
                exit 1
            fi
            PYTHON_INTERPRETOR="$1"
            ;;

    esac
    shift
done

#
# Test to be sure the Python interpretor works and has the curses module
# present.
#
TMP_SCRIPT="$(mktemp /tmp/slurm-output-wrapper-install.XXXXXX)"
cat > "$TMP_SCRIPT" <<EOT
#!${PYTHON_INTERPRETOR}

import curses

EOT
chmod u+x "$TMP_SCRIPT"
TMP_OUTPUT="$("$TMP_SCRIPT" 2>&1)"
TMP_RC=$?
rm -f "$TMP_SCRIPT"
if [ $TMP_RC -ne 0 ]; then
    echo "ERROR:  Python interpretor not working properly:"
    echo "$TMP_OUTPUT"
    exit 1
fi

#
# Copy ssheet_driver into place, prepending the interpretor line:
#
if [ -e "${INSTALL_DIR}/ssheet_driver" ]; then
    echo "ERROR:  Driver already exists: ${INSTALL_DIR}/ssheet_driver"
    exit 1
fi
echo "#!${PYTHON_INTERPRETOR}" > "${INSTALL_DIR}/ssheet_driver"
cat ssheet_driver >> "${INSTALL_DIR}/ssheet_driver"
chmod u+x,g+x,o+x "${INSTALL_DIR}/ssheet_driver"
for util in squeue sinfo sacct sstat sshare; do
    ln -sf ssheet_driver "${INSTALL_DIR}/s${util}"
done

#
# Copy swrap_driver into place, prepending the interpretor line:
#
if [ -e "${INSTALL_DIR}/swrap_driver" ]; then
    echo "ERROR:  Driver already exists: ${INSTALL_DIR}/swrap_driver"
    exit 1
fi
echo "#!${PYTHON_INTERPRETOR}" > "${INSTALL_DIR}/swrap_driver"
cat swrap_driver >> "${INSTALL_DIR}/swrap_driver"
chmod u+x,g+x,o+x "${INSTALL_DIR}/swrap_driver"
for util in squeue sinfo sacct sstat sshare; do
    ln -sf swrap_driver "${INSTALL_DIR}/w${util}"
done

