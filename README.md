# Slurm Output Wrappers

The standard Slurm utilities love to display their output in fixed-width tabular formats.  When a value is wider than the column width, it is truncated and a "+" is displayed as its last character.  One has no choice but to try a different `--format` string with a greater width specified for the offending columns or use the `--parseable` option to have the values displayed untruncated but with a single vertical bar ("|") between each value.  The former generally requires trial and error while the latter leads to listings that are difficult to process visually.

## ssheet_driver

An ideal situation would be to have the output displayed in a tabular format with column widths that adapt to the data present.  Naturally, this can lead to listings that are wider than the typical console.  Managing virtual console windows is the bread-and-butter of the **curses** library, and Python has its own **curses** module that provides bindings to that library.

The `ssheet_driver` Python program wraps several commonly-used Slurm commands (`squeue`, `sinfo`, `sacct`, `sstat`, and `sshare`) and displays the output in a spreadsheet-like format in a scrolling **curses** console window.

>  **NOTE**:  any Slurm utility wrapped by the `ssheet_driver` must accept a `-p/--parseable` argument to generate untruncated output.

The wrapped commands are symbolic links to `ssheet_driver` that are named with an "s" prefix on the original command.  For example, output from `ssqueue --format=%all` looks like this:

```
┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│ACCOUNT         │ GRES   │ MIN_CPUS │ MIN_TMP_DISK │ END_TIME            │ FEATURES │ GROUP           │ OVER_SUBSCRIBE│
│────────────────┼────────┼──────────┼──────────────┼─────────────────────┼──────────┼─────────────────┼───────────────│
│XXXXXXXXXXXX    │ (null) │        1 │ 0            │ 2020-04-30T11:22:30 │ (null)   │ XXXXXXXXXXXX    │ OK            │
│XXXXXXXXXXXX    │ (null) │        1 │ 0            │ 2020-04-30T11:22:29 │ (null)   │ XXXXXXXXXXXX    │ OK            │
│XXXXXX          │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXXX        │ OK            │
│XXXXXXXXXXXXXXX │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXXXXXXXXXX │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│XXXXXXX         │ (null) │        1 │ 0            │ N/A                 │ (null)   │ XXXXXXX         │ OK            │
│──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────│
│ [Q]uit   [P]rev/[N]ext page   Page [L]eft/[R]ight   [E]nd/[B]eginning of list                                        │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```

The P and N keys work like page-up and page-down, and L and R like page-left and page-right.  Single-line and single-character scrolling vertically and horizontally use the arrow keys.  The wrapper accepts no command-line arguments itself, but passes all arguments to the Slurm utility being wrapped.

## swrap_driver

It can also be useful to reformat the parseable output from Slurm utilities in other machine-readable formats.  Having the output as comma- or tab-separated value files makes the output easily imported into other tabular display programs (e.g. Excel).  Utilizing the column headers as keys, presenting the data as JSON or YAML dictionaries is feasible.

Like the `ssheet_driver`, the `swrap_driver` command wraps the same set of Slurm utilities.  The wrapped Slurm commands use a "w" prefix this time and are symbolic links to `swrap_driver`.  There are two command-line arguments accepted by the `swrap_driver` commands:

```
$ wsqueue --help
usage: wsqueue [-h] [--wrap-format {csv,tsv,json,yaml}]
               [--wrap-headers-xform {none,lc,uc,tc}]

Reformat squeue output.

optional arguments:
  -h, --help            show this help message and exit
  --wrap-format {csv,tsv,json,yaml}
                        convert the squeue output to this format (default:
                        csv)
  --wrap-headers-xform {none,lc,uc,tc}
                        transform column header strings (default: none)
```

The desired output format is mandatory.  The header strings can be transformed case-wise, or (for CSV and TSV) omitted completely.  For example:

```
$ wsqueue --wrap-format=csv --wrap-headers-xform=tc
Jobid,Partition,Name,User,St,Time,Nodes,Nodelist(Reason)
8184183_[10-12],XXXXXX,NNNNNNNNN,UUUU,PD,0:00,1,(QOSGrpMemLimit)
8144312_[209-359%30],XXXXXXXXXXXXXXX,NNN,UUUUUU,PD,0:00,1,(JobArrayTaskLimit)
8050903_[0-499],XXXXXXX,NNNNNNNN,UUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8050904_[0-499],XXXXXXX,NNNNNNNN,UUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8050902_[313-499],XXXXXXX,NNNNNNNN,UUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8166805_[0-9],XXXXXXX,NNNNNNNN,UUUUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8166812_[0-9],XXXXXXX,NNNNNNNN,UUUUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8166844_[0-9],XXXXXXX,NNNNNNNN,UUUUUUU,PD,0:00,1,(QOSGrpCpuLimit)
8166945_[0-9],XXXXXXX,NNNNNNNN,UUUUUUU,PD,0:00,1,(QOSGrpCpuLimit)
   :
```

versus YAML format:

```
$ wsqueue --wrap-format=yaml
- JOBID: '8194544_52'
  NAME: NNNNN
  NODELIST(REASON): r01n34
  NODES: '1'
  PARTITION: XXXXXXXXXXX
  ST: CG
  TIME: '7:26'
  USER: UUUUU
- JOBID: 8184183_[10-12]
  NAME: NNNNNNNNN
  NODELIST(REASON): (QOSGrpMemLimit)
  NODES: '1'
  PARTITION: XXXXXX
  ST: PD
  TIME: 0:00
  USER: UUUU
    :
```

## Installation

In short, the two driver programs get installed into a directory and the five "s" or "w" prefixed symbolic links get created.  The directory should be on users' `PATH` for easy access (likewise, the Slurm utilities should also be on the users' `PATH` so they can be accessed as `squeue` or `sinfo`, for example).

A simple `install.sh` script is included that does the work for you:

```
$ ./install.sh --help
usage:

    ./install.sh {options}

  options:

    -h/--help                     display this information
    -i/--installdir <path>        install commands to this directory (default
                                  is /usr/local/bin for root, ~/bin for other
                                  users)
    -p/--python <cmd>             Python interpretor to use in the scripts
                                  (defaults to "/usr/bin/env python")

```

If you have a specific Python interpretor you'd like the driver scripts to use, it's probably a good idea to pass its full path to the `--python` option, e.g.

```
$ ./install --installdir /opt/shared/slurm/add-ons/bin --python /opt/shared/python3/bin/python3
```